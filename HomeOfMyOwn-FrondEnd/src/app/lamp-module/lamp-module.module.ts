import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LampComponent } from './lamp-component/lamp.component';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
  declarations: [LampComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatTabsModule
  ]
})
export class LampModuleModule { }
