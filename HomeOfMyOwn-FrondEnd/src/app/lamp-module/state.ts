export interface State {
  on: boolean;
  bri: number;
  hue: number;
  sat: number;
  xy: number[];
}
