import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Lamp} from './lamp';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LampService {

  constructor(private http: HttpClient) { }

  lampUrl = environment.baseURL + '/lamp';
  testUrl = 'http://www.mocky.io/v2/5e8d98e6310000bf90429a39';

  /**
   * Get all Lamps
   */
  public getLamps() {
    // returns an observable of Lamp
    return this.http.get<Lamp>(this.testUrl);
  }

  /**
   * Get Lamp by id
   * @param id
   */
  public getLamp(id: number) {
    // returns an observable of Lamp
    return this.http.get<Lamp>(this.lampUrl + 'id=' + id);
  }
}
