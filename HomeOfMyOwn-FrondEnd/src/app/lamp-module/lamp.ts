import {State} from './state';
import {PointSymbol} from './point-symbol';

export interface Lamp {
  state: State;
  type: string;
  name: string;
  modelid: string;
  swversion: string;
  pointsymbol: PointSymbol;
}
