import { Component, OnInit } from '@angular/core';
import {Lamp} from '../lamp';
import {LampService} from '../lamp.service';

@Component({
  selector: 'app-lamp-component',
  templateUrl: './lamp.component.html',
  styleUrls: ['./lamp.component.css']
})
export class LampComponent implements OnInit {

  lamp: Lamp;

  constructor(private lampService: LampService ) { }

  ngOnInit(): void {
    this.getLamps();
  }

  getLamps() {
    // voorbeeld werken met observable functie
    this.lampService.getLamps()
      // clone the data object, using its known Lamp shape
      .subscribe((data: Lamp) => this.lamp = { ...data });
  }

}
