import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LampComponent } from './lamp-module/lamp-component/lamp.component';


const routes: Routes = [
  {path: 'lamp', component: LampComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
